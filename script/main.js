let creatTable = document.createElement('table')
let creatTbody = document.createElement('tbody')
let body = document.querySelector('body')

function tableCreate(quantityR, quantityC) {
    for (let row = 0; row < quantityR; row++) {
        let tr = document.createElement('tr')
        for (let cell = 0; cell < quantityC; cell++) {
            if (row === quantityR && cell === quantityC) {
                break
            } else {
                let td = document.createElement('td')
                td.classList.add('white')
                tr.appendChild(td)
            }
        }
        creatTbody.appendChild(tr)
    }
    creatTable.appendChild(creatTbody)
    body.appendChild(creatTable)
}
tableCreate(30, 30)

body.addEventListener("click", (event)=> {
    if (event.target.tagName === 'TD' && event.target.getAttribute('class') === 'white') {
        event.target.classList.add('black')
        event.target.classList.remove('white')
    } else if (event.target.getAttribute('class') === 'black') {
        event.target.classList.remove('black')
        event.target.classList.add('white')
    } else if (event.target.tagName === 'BODY') {
        console.log('white');
        let td = document.querySelectorAll('td')
        td.forEach((el) => {
            if (el.getAttribute('class') === 'white') {
                el.classList.add('black')
                el.classList.remove('white')
            } else {
                el.classList.remove('black')
                el.classList.add('white')
            }
        })
    }
})